﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N3K_WM_EndGrapplingHook : BaseState
{
    private Vector3 hookPoint;
    private Vector3 hookNormal;

    public override void Construct()
    {
        base.Construct();

        immuneTime = 0.25f;
        motor.VerticalVelocity = 5.0f;
        motor.AirInfluence = new Vector3(0f, 5.0f, 0f);
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        // Get the direction in between you and the hookpoint
        input = -((Widowmaker)hero).hookNormal * motor.Speed;
        MotorHelper.ApplyGravity(ref input, ref motor.VerticalVelocity, motor.Gravity, motor.TerminalVelocity);
        MotorHelper.InfluenceAirVelocity(ref input, ref motor.AirInfluence, 0.92f);

        return input;
    }

    public override void PlayerTransition()
    {
        base.PlayerTransition();

        if (Time.time - startTime < immuneTime)
            return;
        if(motor.Grounded())
            motor.ChangeState("N3KWalking");
    }
}
