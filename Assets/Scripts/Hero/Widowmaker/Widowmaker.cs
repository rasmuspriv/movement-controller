﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* ABILITY STATES
 *  0 -> Left Click
 *  1 -> Right Click
 *  2 -> Grappling Hook
 *  3 -> Venom Mine
 *  4 -> Infrasight
 * 
 */

public class Widowmaker : BaseHero
{
    public Vector3 hookPoint;
    public Vector3 hookNormal;

    protected override void CastAbility(Abilities ab)
    {
        switch (ab)
        {
            case Abilities.PrimaryFire:
                abilities[0].Cast();
                break;
            case Abilities.SecondaryFire:
                abilities[1].Cast();
                break;
            case Abilities.Ability1:
                abilities[2].Cast();
                break;
            case Abilities.Ability2:
                abilities[3].Cast();
                break;
            case Abilities.Ability3:
                abilities[4].Cast();
                break;
        }
    }
}
