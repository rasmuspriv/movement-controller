﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_WM_Left : BaseAbility
{
    protected override void Start()
    {
        base.Start();
        cooldown = 0.1f;
    }

    public override void Cast()
    {
        if (IsReady())
        {
            lastCast = Time.time;
            thisHero.Weapon.Fire();
        }
    }
}
