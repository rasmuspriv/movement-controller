﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_GrapplingHook : BaseAbility
{
    private Vector3 hookpoint;
    public Vector3 Hookpoint { get { return hookpoint; } }
    private Vector3 wallNormal;
    public Vector3 WallNormal { get { return wallNormal; } }

    public float missedHookCooldown = 0.5f;
    private const float hookTravelSpeed = 100.0f;

    protected override void Start()
    {
        base.Start();
        lastCast = Time.time - cooldown;
    }

    public override void Cast()
    {
        if (IsReady())
        {
            lastCast = Time.time;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width >> 1, Screen.height >> 1));
            if (Physics.Raycast(ray, out hit, 100))
            {
                // Shall we snap the hook to the highest point ? $$

                hookpoint = hit.point;
                wallNormal = hit.normal;
                (thisHero as Widowmaker).hookPoint = hookpoint;
                (thisHero as Widowmaker).hookNormal = wallNormal;
                
                Debug.DrawRay(ray.origin, ray.direction * (hookpoint - ray.origin).magnitude, Color.green, 3.0f);

                // Launch the visual hook, and send him information
                ProjectileManager.Instance.LaunchProjectile(ProjectileType.WM_GrapplingHook, OnHit, GetHero(), ray.origin, (hookpoint - ray.origin).normalized, hookTravelSpeed);
            }
            else
            {
                lastCast = Time.time - cooldown + missedHookCooldown;
                hookpoint = Vector3.zero;
                wallNormal = Vector3.zero;
            }
        }
    }

    private void OnHit()
    {
        GetComponent<BaseMotor>().ChangeState("N3K_WM_ActiveGrapplingHook");
    }
}
