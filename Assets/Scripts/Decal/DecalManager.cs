﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalManager : MonoSingleton<DecalManager>
{
    private const float DISTANCE_FROM_WALL = 0.005f;

    [SerializeField]
    private GameObject decalPrefab;

    [SerializeField]
    private int maxConcurrentDecals = 10;

    private Queue<GameObject> decalsInPool;
    private Queue<GameObject> decalsActiveInWorld;

    private void Awake()
    {
        InitializeDecals();
    }

    private void InitializeDecals()
    {
        decalsInPool = new Queue<GameObject>();
        decalsActiveInWorld = new Queue<GameObject>();

        for (int i = 0; i < maxConcurrentDecals; i++)
        {
            InstantiateDecal();
        }
    }

    private void InstantiateDecal()
    {
        var spawned = GameObject.Instantiate(decalPrefab);
        spawned.transform.SetParent(this.transform);

        decalsInPool.Enqueue(spawned);
        spawned.SetActive(false);
    }

    public void SpawnDecal(RaycastHit hit)
    {
        GameObject decal = GetNextAvailableDecal();
        if (decal != null)
        {
            decal.transform.position = (hit.point + hit.normal * DISTANCE_FROM_WALL);
            decal.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

            decal.SetActive(true);

            decalsActiveInWorld.Enqueue(decal);
        }
    }

    private GameObject GetNextAvailableDecal()
    {
        if (decalsInPool.Count > 0)
            return decalsInPool.Dequeue();

        var oldestActiveDecal = decalsActiveInWorld.Dequeue();
        return oldestActiveDecal;
    }
}
