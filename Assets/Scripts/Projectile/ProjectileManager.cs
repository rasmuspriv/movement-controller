﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProjectileType
{
    WM_GrapplingHook = 230,
    WM_VenomMine = 231,
}

public class ProjectileManager : MonoSingleton<ProjectileManager>
{
    public List<Projectile> projectiles = new List<Projectile>();

    public void LaunchProjectile(ProjectileType pType,Action action, BaseHero sender, Vector3 startPos, Vector3 dir, float speed)
    {
        // Pool the projectile
        Projectile p = projectiles.Find(x => x.type == pType && !x.isActive);

        // Projectile does not exist, let's create it
        if (p == null)
        {
            p = CreateProjectile(pType);
            projectiles.Add(p);
        }

        ProjectileInfo pi = new ProjectileInfo { type = pType, collision = false, startPosition = startPos, direction = dir, speed = speed, gravity = 0, sender = sender };
        p.Launch(pi,action);
    }

    public Projectile CreateProjectile(ProjectileType type)
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Projectile p = go.AddComponent<Projectile>();
        p.rigid = p.gameObject.AddComponent<Rigidbody>();
        p.type = type;
        p.rigid.useGravity = false;
        p.GetComponent<Collider>().isTrigger = true;

        return p;
    }

    private void FixedUpdate()
    {
        foreach (Projectile p in projectiles)
        {
            if (p.isActive)
            {
                p.UpdateProjectile();
            }
        }
    }
}
