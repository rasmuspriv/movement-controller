﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ProjectileInfo
{
    public ProjectileType type;
    public bool collision;
    public Vector3 startPosition,direction;
    public float speed, gravity;
    public BaseHero sender;
}

public class Projectile : MonoBehaviour
{
    private const float SPEED_TO_METER = 60.0f;

    public GameObject thisGameObject;
    public Transform thisTransform;
    public Collider thisCollider;
    public Rigidbody rigid;

    public ProjectileType type;
    public BaseHero sender;
    public float speed;
    public bool isActive;
    public Vector3 direction;

    // Action
    private Action onHit;
    public void SetActionOnHit(Action action)
    {
        this.onHit = action;
    }

    private void Awake()
    {
        thisTransform = transform;
        thisGameObject = gameObject;
        rigid = GetComponent<Rigidbody>();
        thisCollider = GetComponent<Collider>();
    }

    public void Launch(ProjectileInfo info,Action action)
    {
        gameObject.SetActive(true);
        type = info.type;
        sender = info.sender;
        direction = info.direction;
        speed = info.speed;
        thisTransform.position = info.startPosition;
        isActive = true;
        SetActionOnHit(action);
        Physics.IgnoreCollision(info.sender.GetComponent<Collider>(), thisCollider);
    }

    public void Stop()
    {
        isActive = false;
        gameObject.SetActive(false);
    }

    public void UpdateProjectile()
    {
        if (isActive)
        {
            Vector3 moveDelta = direction * speed;
            rigid.velocity = moveDelta;
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (onHit != null)
        {
            onHit.Invoke();
            onHit = null;
            Stop();
        }
    }
}
