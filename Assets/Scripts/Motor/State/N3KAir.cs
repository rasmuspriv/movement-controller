﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N3KAir : BaseState
{
    public override void Construct()
    {
        base.Construct();
        immuneTime = 0.1f;
    }

    public override void Destruct()
    {
        base.Destruct();
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        MotorHelper.KillVector(ref input, motor.WallVector);
        MotorHelper.ApplySpeed(ref input, motor.Speed);
        MotorHelper.ApplyGravity(ref input, ref motor.VerticalVelocity, motor.Gravity, motor.TerminalVelocity);
        MotorHelper.InfluenceAirVelocity(ref input, ref motor.AirInfluence, 0.97f);

        return input;
    }

    public override void PlayerTransition()
    {
        base.PlayerTransition();

        if (motor.Grounded())
            motor.ChangeState("N3KWalking");
    }
}
